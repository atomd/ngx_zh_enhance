Usage
-----
Usage is the combination of [rewrite_if](http://wiki.nginx.org/HttpRewriteModule#if "nginx wiki") and [rewrite_set](http://wiki.nginx.org/HttpRewriteModule#set "nginx wiki").

    zh_set_if (condition) $old $new;

Working example::

        default_type text/html;
        set $true 1;
        
        zh_set_if ($true) $atomd 'hello world';
        zh_set_if ($http_user_agent ~* 'iphone|android') $is_moblie 1;
        zh_set_if ($cookie_KEY) $is_cookie 1;
        
        echo $atomd;
        echo 'mobile:$is_moblie';
        echo 'cookie:$is_cookie';

Installation
------------

    ./configure --add-module=/path/to/ngx_devel_kit --add-module=/path/to/ngx_http_zh_enhance_module

