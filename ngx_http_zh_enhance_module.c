#include <ndk.h>

typedef struct {
    ngx_http_script_code_pt     code;
    ngx_uint_t                  next;
} ngx_http_zh_set_if_code_t;

static char *ngx_http_zh_set_if(ngx_conf_t *cf,
                                ngx_command_t *cmd,
                                void *conf);
static char *ngx_http_zh_if_condition(ngx_conf_t *cf,
                                      ndk_http_rewrite_loc_conf_t *lcf);

static char *ngx_http_zh_set_condition(ngx_conf_t *cf,
                                       ndk_http_rewrite_loc_conf_t *lcf);

void ngx_http_script_zh_set_if_code(ngx_http_script_engine_t *e);

static ngx_command_t  ngx_http_zh_enhance_commands[] = {
    {
        ngx_string("zh_set_if"),
        NGX_HTTP_MAIN_CONF|NGX_HTTP_SRV_CONF|NGX_HTTP_SIF_CONF|
            NGX_HTTP_LOC_CONF|NGX_HTTP_LIF_CONF|NGX_CONF_2MORE,
        ngx_http_zh_set_if,
        NGX_HTTP_LOC_CONF_OFFSET,
        0,
        NULL 
    },
    ngx_null_command
};

static ngx_http_module_t  ngx_http_zh_enhance_module_ctx = {
    NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
};

ngx_module_t  ngx_http_zh_enhance_module = {
    NGX_MODULE_V1,
    &ngx_http_zh_enhance_module_ctx,    /* module context */
    ngx_http_zh_enhance_commands,       /* module directives */
    NGX_HTTP_MODULE,                    /* module type */
    NULL,                               /* init master */
    NULL,                               /* init module */
    NULL,                               /* init process */
    NULL,                               /* init thread */
    NULL,                               /* exit thread */
    NULL,                               /* exit process */
    NULL,                               /* exit master */
    NGX_MODULE_V1_PADDING
};

static char *
ngx_http_zh_set_if(ngx_conf_t *cf, ngx_command_t *cmd, void *conf)
{
    ndk_http_rewrite_loc_conf_t     *rlcf;
    ngx_http_zh_set_if_code_t       *if_code;
    u_char                          *elts;

    if (cf->args->nelts < 4) {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "invalid number of the zh_set_if parameters");
        return NGX_CONF_ERROR;
    }

    rlcf = ngx_http_conf_get_module_loc_conf(cf,
                                             ngx_http_rewrite_module);

    if (ngx_http_zh_if_condition(cf, rlcf) != NGX_CONF_OK) {
        return NGX_CONF_ERROR;
    }

    if_code = ngx_http_script_start_code(cf->pool, &rlcf->codes,
            sizeof(ngx_http_zh_set_if_code_t));
    if (if_code == NULL) {
        return NGX_CONF_ERROR;
    }

    if_code->code = ngx_http_script_zh_set_if_code;
    elts = rlcf->codes->elts;

    if ( ngx_http_zh_set_condition(cf, rlcf) != NGX_CONF_OK) {
        return NGX_CONF_ERROR;
    }

    if (elts != rlcf->codes->elts) {
        if_code = (ngx_http_zh_set_if_code_t *) ((u_char *) if_code
                  + ((u_char *) rlcf->codes->elts - elts));
    }
    if_code->next = (u_char *) rlcf->codes->elts + rlcf->codes->nelts
                                                - (u_char *) if_code;

    return NGX_CONF_OK;
}

void ngx_http_script_zh_set_if_code(ngx_http_script_engine_t *e)
{
    ngx_http_zh_set_if_code_t *code;

    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, e->request->connection->log, 0,
                   "http script zh_set_if");
    code = (ngx_http_zh_set_if_code_t *) e->ip;
    e->sp--;

    if (e->sp->len && (e->sp->len !=1 || e->sp->data[0] != '0')) {
        e->ip += sizeof(ngx_http_zh_set_if_code_t);
        ngx_log_debug0(NGX_LOG_DEBUG_HTTP, e->request->connection->log, 0,
                   "http script zh_set_if: true, set");
        return;
    }

    ngx_log_debug0(NGX_LOG_DEBUG_HTTP, e->request->connection->log, 0,
                   "http script zh_set_if: false, no set");
    e->ip += code->next;
}

//copy and modify from ngx_http_rewrite_module
//use ndk_http_rewrite_value instead of ngx_http_rewrite_variable 
static char *
ngx_http_zh_if_condition(ngx_conf_t *cf,
                         ndk_http_rewrite_loc_conf_t *lcf)
{
    u_char                        *p;
    size_t                         len;
    ngx_str_t                     *value;
    ngx_int_t                      cur, last;
    ngx_regex_compile_t            rc;
    ngx_http_script_code_pt       *code;
    ngx_http_script_file_code_t   *fop;
    ngx_http_script_regex_code_t  *regex;
    u_char                         errstr[NGX_MAX_CONF_ERRSTR];

    value = cf->args->elts;
    last = cf->args->nelts - 3;

    if (value[1].len < 1 || value[1].data[0] != '(') {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "invalid condition \"%V\"", &value[1]);
        return NGX_CONF_ERROR;
    }

    if (value[1].len == 1) {
        cur = 2;

    } else {
        cur = 1;
        value[1].len--;
        value[1].data++;
    }

    if (value[last].len < 1 || value[last].data[value[last].len - 1] != ')') {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "invalid condition \"%V\"", &value[last]);
        return NGX_CONF_ERROR;
    }

    if (value[last].len == 1) {
        last--;

    } else {
        value[last].len--;
        value[last].data[value[last].len] = '\0';
    }

    len = value[cur].len;
    p = value[cur].data;

    if (len > 1 && p[0] == '$') {

        if (cur != last && cur + 2 != last) {
            ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                               "invalid condition \"%V\"", &value[cur]);
            return NGX_CONF_ERROR;
        }

        if (ndk_http_rewrite_value(cf, lcf, &value[cur]) != NGX_CONF_OK) {
            return NGX_CONF_ERROR;
        }

        if (cur == last) {
            return NGX_CONF_OK;
        }

        cur++;

        len = value[cur].len;
        p = value[cur].data;

        if (len == 1 && p[0] == '=') {

            if (ndk_http_rewrite_value(cf, lcf, &value[last]) != NGX_CONF_OK) {
                return NGX_CONF_ERROR;
            }

            code = ngx_http_script_start_code(cf->pool, &lcf->codes,
                                              sizeof(uintptr_t));
            if (code == NULL) {
                return NGX_CONF_ERROR;
            }

            *code = ngx_http_script_equal_code;

            return NGX_CONF_OK;
        }

        if (len == 2 && p[0] == '!' && p[1] == '=') {

            if (ndk_http_rewrite_value(cf, lcf, &value[last]) != NGX_CONF_OK) {
                return NGX_CONF_ERROR;
            }

            code = ngx_http_script_start_code(cf->pool, &lcf->codes,
                                              sizeof(uintptr_t));
            if (code == NULL) {
                return NGX_CONF_ERROR;
            }

            *code = ngx_http_script_not_equal_code;
            return NGX_CONF_OK;
        }

        if ((len == 1 && p[0] == '~')
            || (len == 2 && p[0] == '~' && p[1] == '*')
            || (len == 2 && p[0] == '!' && p[1] == '~')
            || (len == 3 && p[0] == '!' && p[1] == '~' && p[2] == '*'))
        {
            regex = ngx_http_script_start_code(cf->pool, &lcf->codes,
                                         sizeof(ngx_http_script_regex_code_t));
            if (regex == NULL) {
                return NGX_CONF_ERROR;
            }

            ngx_memzero(regex, sizeof(ngx_http_script_regex_code_t));

            ngx_memzero(&rc, sizeof(ngx_regex_compile_t));

            rc.pattern = value[last];
            rc.options = (p[len - 1] == '*') ? NGX_REGEX_CASELESS : 0;
            rc.err.len = NGX_MAX_CONF_ERRSTR;
            rc.err.data = errstr;

            regex->regex = ngx_http_regex_compile(cf, &rc);
            if (regex->regex == NULL) {
                return NGX_CONF_ERROR;
            }

            regex->code = ngx_http_script_regex_start_code;
            regex->next = sizeof(ngx_http_script_regex_code_t);
            regex->test = 1;
            if (p[0] == '!') {
                regex->negative_test = 1;
            }
            regex->name = value[last];

            return NGX_CONF_OK;
        }

        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "unexpected \"%V\" in condition", &value[cur]);
        return NGX_CONF_ERROR;

    } else if ((len == 2 && p[0] == '-')
               || (len == 3 && p[0] == '!' && p[1] == '-'))
    {
        if (cur + 1 != last) {
            ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                               "invalid condition \"%V\"", &value[cur]);
            return NGX_CONF_ERROR;
        }

        value[last].data[value[last].len] = '\0';
        value[last].len++;

        if (ndk_http_rewrite_value(cf, lcf, &value[last]) != NGX_CONF_OK) {
            return NGX_CONF_ERROR;
        }

        fop = ngx_http_script_start_code(cf->pool, &lcf->codes,
                                          sizeof(ngx_http_script_file_code_t));
        if (fop == NULL) {
            return NGX_CONF_ERROR;
        }

        fop->code = ngx_http_script_file_code;

        if (p[1] == 'f') {
            fop->op = ngx_http_script_file_plain;
            return NGX_CONF_OK;
        }

        if (p[1] == 'd') {
            fop->op = ngx_http_script_file_dir;
            return NGX_CONF_OK;
        }

        if (p[1] == 'e') {
            fop->op = ngx_http_script_file_exists;
            return NGX_CONF_OK;
        }

        if (p[1] == 'x') {
            fop->op = ngx_http_script_file_exec;
            return NGX_CONF_OK;
        }

        if (p[0] == '!') {
            if (p[2] == 'f') {
                fop->op = ngx_http_script_file_not_plain;
                return NGX_CONF_OK;
            }

            if (p[2] == 'd') {
                fop->op = ngx_http_script_file_not_dir;
                return NGX_CONF_OK;
            }

            if (p[2] == 'e') {
                fop->op = ngx_http_script_file_not_exists;
                return NGX_CONF_OK;
            }

            if (p[2] == 'x') {
                fop->op = ngx_http_script_file_not_exec;
                return NGX_CONF_OK;
            }
        }

        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "invalid condition \"%V\"", &value[cur]);
        return NGX_CONF_ERROR;
    }

    ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                       "invalid condition \"%V\"", &value[cur]);

    return NGX_CONF_ERROR;
}


//copy and modify from ngx_http_rewrite_module
//use ndk_http_rewrite_value instead of ngx_http_rewrite_variable 
//use ndk_http_rewrite_var instead of ngx_http_rewrite_var
static char *
ngx_http_zh_set_condition(ngx_conf_t *cf,
                          ndk_http_rewrite_loc_conf_t *lcf)
{
    ngx_int_t                            index, cur;
    ngx_str_t                           *value;
    ngx_http_variable_t                 *v;
    ngx_http_script_var_code_t          *vcode;
    ngx_http_script_var_handler_code_t  *vhcode;

    value = cf->args->elts;
    cur = cf->args->nelts - 2;

    if (value[cur].data[0] != '$') {
        ngx_conf_log_error(NGX_LOG_EMERG, cf, 0,
                           "invalid variable name \"%V\"", &value[1]);
        return NGX_CONF_ERROR;
    }

    value[cur].len--;
    value[cur].data++;

    v = ngx_http_add_variable(cf, &value[cur], NGX_HTTP_VAR_CHANGEABLE);
    if (v == NULL) {
        return NGX_CONF_ERROR;
    }

    index = ngx_http_get_variable_index(cf, &value[cur]);
    if (index == NGX_ERROR) {
        return NGX_CONF_ERROR;
    }

    if (v->get_handler == NULL
        && ngx_strncasecmp(value[cur].data, (u_char *) "http_", 5) != 0
        && ngx_strncasecmp(value[cur].data, (u_char *) "sent_http_", 10) != 0
        && ngx_strncasecmp(value[cur].data, (u_char *) "upstream_http_", 14) != 0)
    {
        v->get_handler = ndk_http_rewrite_var;
        v->data = index;
    }

    //ngx_http_script_value_code_t or ngx_http_script_complex_value_code
    if (ndk_http_rewrite_value(cf, lcf, &value[cur+1]) != NGX_CONF_OK) {
        return NGX_CONF_ERROR;
    }

    if (v->set_handler) {
        vhcode = ngx_http_script_start_code(cf->pool, &lcf->codes,
                                   sizeof(ngx_http_script_var_handler_code_t));
        if (vhcode == NULL) {
            return NGX_CONF_ERROR;
        }

        vhcode->code = ngx_http_script_var_set_handler_code;
        vhcode->handler = v->set_handler;
        vhcode->data = v->data;

        return NGX_CONF_OK;
    }

    vcode = ngx_http_script_start_code(cf->pool, &lcf->codes,
                                       sizeof(ngx_http_script_var_code_t));
    if (vcode == NULL) {
        return NGX_CONF_ERROR;
    }

    vcode->code = ngx_http_script_set_var_code;
    vcode->index = (uintptr_t) index;

    return NGX_CONF_OK;
}
